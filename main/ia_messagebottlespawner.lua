-------------------------for new ia_messagebottle spawn-------------
local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

IAENV.AddPrefabPostInit("forest", function(inst)
    local function SpawnMesssagebottle()
        local x, y
        local map = TheWorld.Map
        local sx, sy = map:GetSize()
        local i = 1
        repeat
            x = math.random(-sx,sx)
            y = math.random(-sy,sy)
            local tile = map:GetTileAtPoint(x, 0, y)
            i = i + 1
        until  IsWaterTile(tile) or i == 1000
        SpawnPrefab("ia_messagebottle").Transform:SetPosition(x, 0, y)
    end

    if TheWorld.ismastersim then
        if not inst:HasTag("volcano") then
            inst:ListenForEvent("timerdone", SpawnMesssagebottle)
        end
    end
end)