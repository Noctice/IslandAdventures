local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

----------------------------------------------------------------------------------------

local CraftingMenuWidget = require("widgets/redux/craftingmenu_widget")

local _UpdateFilterButtons = CraftingMenuWidget.UpdateFilterButtons

local function Refresh_CraftingMenuWidget(self)
    local rebuild_details_list = _UpdateFilterButtons
	if self.sort_class.Refresh == nil or not self.sort_class:Refresh() then
		if rebuild_details_list then
			self:ApplyFilters()
		else
			self.recipe_grid:RefreshView()
		end
		if self.crafting_hud:IsCraftingOpen() then
			self:OnCraftingMenuOpen(true)
		end
	end
	self.details_root:Refresh()
end

local jellybrainhat_change = false
function CraftingMenuWidget:UpdateFilterButtons(...)
    _UpdateFilterButtons(self, ...)

    local builder = self.owner ~= nil and self.owner.replica.builder or nil
    if builder and builder.classified.isjellybrainhat:value() then
        self.crafting_station_filter:Show()
        Refresh_CraftingMenuWidget(self)

        jellybrainhat_change = true
    elseif jellybrainhat_change then
        self.crafting_station_filter:Hide()
        Refresh_CraftingMenuWidget(self)

        jellybrainhat_change = false
    end

    -- fishingrod change with world
    STRINGS.NAMES.FISHINGROD = TheWorld:HasTag("island") and STRINGS.NAMES.IA_FISHINGROD or STRINGS.NAMES.DST_FISHINGROD
    STRINGS.NAMES.OCEANFISHINGROD = TheWorld:HasTag("island") and STRINGS.NAMES.IA_OCEANFISHINGROD or STRINGS.NAMES.DST_OCEANFISHINGROD

end