local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

----------------------------------------------------------------------------------------

local function startwereplayer(inst, data)
	if inst.components.poisonable then
		inst.components.poisonable:SetBlockAll(true)
	end
	inst:AddTag("NOVACUUM")
	inst.components.health.cantdrown = true
end

------

local function newstate(inst, data)
	--data.statename
	if inst:HasTag("idle") then
		if inst._embarkingboat and inst._embarkingboat:IsValid() then
			inst.components.sailor:Embark(inst._embarkingboat)
			inst._embarkingboat = nil
		end
		inst:RemoveEventCallback("newstate", newstate)
	end
end

local function stop(inst)
	if inst.Physics then
		inst.Physics:Stop()
	end
end

local function stopwereplayer(inst, data)
	if inst.components.poisonable and not inst:HasTag("playerghost") then
		inst.components.poisonable:SetBlockAll(false)
	end
	if  inst:HasTag("NOVACUUM") then
		inst:RemoveTag("NOVACUUM")
	end
	inst.components.health.cantdrown = false
	------
	inst.Physics:CollidesWith(COLLISION.WAVES) --weregoose resets collision mask in SetWereDrowning
	--copy from postinit/player.lua to place weregoose on boat
	if IsOnWater(inst) and inst.components.sailor then
		local boat = FindEntity(inst, 3, nil, {"sailable"}, {"INLIMBO", "fire", "NOCLICK"})
		if boat then
			boat.components.sailable.isembarking = true
			inst._embarkingboat = boat
			--Move there!
			inst:ForceFacePoint(boat:GetPosition():Get())
			local dist = inst:GetPosition():Dist(boat:GetPosition())
			inst.Physics:SetMotorVelOverride(dist / .8, 0, 0)
			inst:DoTaskInTime(.8, stop)
			--Drowning immunity appears to be not needed. -M
			inst:ListenForEvent("newstate", newstate)
		end
	end
end

----------------------------------------------------------------------------------------
--Try to initialise all functions locally outside of the post-init so they exist in RAM only once
----------------------------------------------------------------------------------------

IAENV.AddPrefabPostInit("woodie", function(inst)


if TheWorld.ismastersim then

	inst:ListenForEvent("startwereplayer", startwereplayer)
	inst:ListenForEvent("stopwereplayer", stopwereplayer)
	
end


end)
