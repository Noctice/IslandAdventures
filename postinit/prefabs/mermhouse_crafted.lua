local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

----------------------------

local sw_loot_merm = 
{
    "fish_tropical",
}

local original_function_spawn
local function new_function_spawn(inst, child)
    if not inst:HasTag("burnt") then
        child.components.lootdropper:SetLoot(sw_loot_merm)
        child:AddTag("save_loot")
    end
    original_function_spawn(inst, child)
end
local function Loot_Check(inst)

    inst:AddTag("mermhouse")
    
    if TheWorld.ismastersim and IsInIAClimate(inst) then
        if not original_function_spawn then
            original_function_spawn = inst.components.childspawner.onspawned
        end
        inst.components.childspawner.onspawned = new_function_spawn
    end
end

IAENV.AddPrefabPostInit("mermwatchtower", Loot_Check)
IAENV.AddPrefabPostInit("mermhouse_crafted", Loot_Check)
