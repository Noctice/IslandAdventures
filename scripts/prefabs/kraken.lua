
local assets =
{
    Asset("ANIM", "anim/quacken.zip"),
}

local prefabs =
{
    "kraken_tentacle",
    "kraken_projectile",
    "kraken_inkpatch",
    "krakenchest",
}

SetSharedLootTable('kraken',
{
    {"piratepack", 1.00},
})

local MIN_HEALTH =
{
	0.75,
	0.50,
	0.25,
	-1.0,
}

local MaxHealth = TUNING.QUACKEN_HEALTH

local function MoveToNewSpot(inst)  --25%血移动
	local pos = inst:GetPosition()
	local new_pos = pos
	for i=1, 50 do
		local offset = FindWaterOffset(pos, math.pi * 2 * math.random(), 40, 30)
		new_pos = pos + offset
		local tile = TheWorld.Map:GetTileAtPoint(new_pos:Get())
		if tile == GROUND.OCEAN_DEEP or tile == GROUND.OCEAN_SHIPGRAVEYARD  then --在深海和海墓
			break
		end
    end
	inst:PushEvent("move", {pos = new_pos})
end

local function OnMinHealth(inst, data)
    if not inst.components.health:IsDead() then
        inst.health_stage = inst.health_stage + 1
        inst.health_stage = math.min(inst.health_stage, #MIN_HEALTH)
        inst.components.health:SetMinHealth(MaxHealth * MIN_HEALTH[inst.health_stage])
        MoveToNewSpot(inst)
    end
end

local RND_OFFSET = 10

local function OnAttack(inst, data)
	local numshots = 3

	if data.target then
		for i = 1, numshots do
			local offset = Vector3(math.random(-RND_OFFSET, RND_OFFSET), math.random(-RND_OFFSET, RND_OFFSET), math.random(-RND_OFFSET, RND_OFFSET))
			inst.components.thrower:Throw(data.target:GetPosition() + offset)
		end
	end
end

local function Retarget(inst)   --重定目标函数
    return FindEntity(inst, 40, function(guy)
        if guy.components.combat and guy.components.health and not guy.components.health:IsDead() then
            return not (guy.prefab == inst.prefab)
        end
    end, nil, {"prey"}, {"character", "monster", "animal"})
end

local function ShouldKeepTarget(inst, target)
    if target and target:IsValid() and target.components.health and not target.components.health:IsDead() then
        local distsq = target:GetDistanceSqToInst(inst)  --获取实体玩家距离
        return distsq < 1600
    else
        return false
    end
end

local function SpawnChest(inst)
    inst:DoTaskInTime(3, function()
		inst.SoundEmitter:PlaySound("dontstarve/common/ghost_spawn")

		local chest = SpawnPrefab("krakenchest")
		local pos = inst:GetPosition()
		chest.Transform:SetPosition(pos.x, 0, pos.z)

		local fx = SpawnPrefab("statue_transition_2")
		if fx then
			fx.Transform:SetPosition(inst:GetPosition():Get())
			fx.Transform:SetScale(1, 2, 1)
		end

		fx = SpawnPrefab("statue_transition")
		if fx then
			fx.Transform:SetPosition(inst:GetPosition():Get())
			fx.Transform:SetScale(1, 1.5, 1)
		end

		chest:AddComponent("scenariorunner")
		chest.components.scenariorunner:SetScript("chest_kraken")
		chest.components.scenariorunner:Run()
	end)
end

local function OnRemove(inst)
    inst.components.minionspawner:DespawnAll()
end

local function OnSave(inst, data)
    data.health_stage = inst.health_stage
end

local function OnLoad(inst, data)
    if data and data.health_stage then
        inst.health_stage = data.health_stage or inst.health_stage
        inst.components.health:SetMinHealth(MaxHealth * MIN_HEALTH[inst.health_stage])
    end
end

------------------------minionspawner change --------------------------
local function AddPosition(self, num)
	table.insert(self.freepositions, num)
	table.sort(self.freepositions)
end

local function OnLostMinion(self, minion)
	if self.minions[minion] == nil then
		return
	end

	self:AddPosition(minion.minionnumber)

	self.minions[minion] = nil
	self.numminions = self.numminions - 1

	self.inst:RemoveEventCallback("attacked", self._onminionattacked, minion)
	self.inst:RemoveEventCallback("onattackother", self._onminionattack, minion)
	self.inst:RemoveEventCallback("death", self._onminiondeath, minion)
	self.inst:RemoveEventCallback("onremove", self._onminionremoved, minion)

	self.inst:PushEvent("minionchange")

	if self.shouldspawn and not self:MaxedMinions() then
		self:StartNextSpawn()
	end
end

local function SpawnNewMinion(self, force)
    if self.minionpositions == nil then
        self.minionpositions = self:MakeSpawnLocations()
        if self.minionpositions == nil then
            return
        end
    end

    if (force or self.shouldspawn) and not self:MaxedMinions() and #self.freepositions > 0 then
        self.spawninprogress = false

        local num = self.freepositions[math.random(#self.freepositions)]
        local pos = self:GetSpawnLocation(num)
        if pos ~= nil then
            local minion = self:MakeMinion()
            if minion ~= nil then
                minion.sg:GoToState("spawn")
                minion.minionnumber = num
                self:TakeOwnership(minion)
                minion.Transform:SetPosition(pos:Get())
                self:RemovePosition(num)

                if self.onspawnminionfn ~= nil then
                    self.onspawnminionfn(self.inst, minion)
                end
            end
        elseif self.miniontype ~= nil and not self:MaxedMinions() then
            self.minionpositions = self:MakeSpawnLocations()
        end

        if (force or self.shouldspawn) and not self:MaxedMinions() then
            self:StartNextSpawn()
        end
    end
end

-----------------------------------------------------------------------


local function fn()

	local inst = CreateEntity()  --实体
	local trans = inst.entity:AddTransform()  --变换
	local anim = inst.entity:AddAnimState()  --动画
	local sound = inst.entity:AddSoundEmitter()  --声音
	inst.entity:AddNetwork() --添加网络

	inst.entity:AddMiniMapEntity()  --小地图
	inst.MiniMapEntity:SetIcon("quacken.tex")
	inst.MiniMapEntity:SetPriority(4)

	MakeCharacterPhysics(inst, 1000, 1)  --物理特性组件，这个函数设置了物理、碰撞标签、碰撞掩码、摩擦力

    anim:SetBank("quacken")
    anim:SetBuild("quacken")
    anim:PlayAnimation("idle_loop", true)

	inst:AddTag("kraken")
    inst:AddTag("nowaves")
    inst:AddTag("epic")
    inst:AddTag("noteleport")
	inst:AddTag("mudacamada")

	inst.entity:SetPristine()

        if not TheWorld.ismastersim then
        return inst
    end

	inst:AddComponent("inspectable")  --可检查
	inst:AddComponent("health")  --生命组件
	inst.components.health:SetMaxHealth(MaxHealth)  --最大生命
	inst.components.health.nofadeout = true

	inst:AddComponent("combat")  --攻击组件
	inst.components.combat:SetDefaultDamage(0) --攻击力
	inst.components.combat:SetAttackPeriod(TUNING.QUACKEN_ATTACK_PERIOD)  --攻速
	inst.components.combat:SetRange(40, 50)     --攻击范围
	inst.components.combat:SetRetargetFunction(1, Retarget)
	inst.components.combat:SetKeepTargetFunction(ShouldKeepTarget)  --保持目标
	inst:AddComponent("locomotor")

	inst:AddComponent("sanityaura")

	inst:AddComponent("lootdropper")--设置掉落组件
	inst.components.lootdropper:SetChanceLootTable('kraken') --掉落表

	inst:AddComponent("thrower")   --吐东西组件
	inst.components.thrower.throwable_prefab = "kraken_projectile"

	inst:SetStateGraph("SGkraken")   --状态

    local brain = require("brains/krakenbrain")
	inst:SetBrain(brain)

	inst.health_stage = 1

    inst:ListenForEvent("minhealth", OnMinHealth)  --最小生命值，转移时无敌
    inst.components.health:SetMinHealth(MaxHealth * MIN_HEALTH[inst.health_stage])
    inst:ListenForEvent("death", SpawnChest)   --死亡掉箱子
    inst:ListenForEvent("onattackother", OnAttack)
    inst:ListenForEvent("onremove", OnRemove)

    inst:AddComponent("minionspawner")  --触手生成
    inst.components.minionspawner.validtiletypes = {
		[GROUND.OCEAN_SHALLOW] = true,
		[GROUND.OCEAN_MEDIUM] = true,
		[GROUND.OCEAN_DEEP] = true,
		[GROUND.OCEAN_CORAL] = true,
		[GROUND.MANGROVE] = true,
		[GROUND.OCEAN_SHIPGRAVEYARD] = true
	}
    inst.components.minionspawner.miniontype = "kraken_tentacle"
    inst.components.minionspawner.distancemodifier = TUNING.QUACKEN_TENTACLE_DIST_MOD  --35
    inst.components.minionspawner.maxminions = TUNING.QUACKEN_MAXTENTACLES  --45
	inst.components.minionspawner:RegenerateFreePositions()
	inst.components.minionspawner.shouldspawn = false
	inst.components.minionspawner._onminionremoved = inst.components.minionspawner._onminiondeath
	inst.components.minionspawner.AddPosition = AddPosition
	inst.components.minionspawner.OnLostMinion = OnLostMinion
	inst.components.minionspawner.SpawnNewMinion = SpawnNewMinion

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	return inst
end

-------------------------------for kraken appear music, I really don't want to write that-------------------------------
function GetStartDangerMusicFunction()
    local StartPlayerListeners
    for i, v in ipairs(TheWorld.event_listening["playeractivated"][TheWorld]) do
        if UpvalueHacker.GetUpvalue(v, "StartPlayerListeners") then
            StartPlayerListeners = UpvalueHacker.GetUpvalue(v, "StartPlayerListeners")
            break
        end
    end

    if StartPlayerListeners then
        local OnAttacked = UpvalueHacker.GetUpvalue(StartPlayerListeners, "OnAttacked")
        if OnAttacked then
            local StartDanger = UpvalueHacker.GetUpvalue(OnAttacked, "StartDanger")
            return StartDanger
        end
    end
end

local function OnKrakenMusicDirty(inst)
	local player = inst.krakenmusic:value()
	local StartDangerMusicFunction = GetStartDangerMusicFunction()
	if player and StartDangerMusicFunction then
		StartDangerMusicFunction(player)
	end
end

local function music()
	local inst = CreateEntity()
	inst.entity:AddTransform()
	inst.entity:AddNetwork()

	inst:AddTag("kraken")
	inst:AddTag("nowaves")
	inst:AddTag("epic")
	inst:AddTag("noteleport")
	inst:AddTag("mudacamada")

	inst.krakenmusic = net_entity(inst.GUID, "kraken.music", "krakenmusicdirty")

	if not TheNet:IsDedicated() then
		inst:ListenForEvent("krakenmusicdirty", OnKrakenMusicDirty)
	end

	inst:DoTaskInTime(10, inst.Remove)

	return inst
end

return Prefab( "monsters/kraken", fn, assets, prefabs),
		Prefab("krakenmusic", music)

